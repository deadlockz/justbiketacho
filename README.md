# just a Bike Tacho

Build your own bicycle tacho with arduino pro mini,  TM1637 LED Display (4digits, 7 segments) and a
hall effect sensor 49E 822 8G.

Code is for km/h - nothing more! Feel free to expand it with our own features.

## Why?

I love these old LED Displays without any graphics or menu stuff. Keep it simple!

## How?

I do not want to use a reed sensor with interups because I had only some hall sensors
in my drawer. Thus I get no clear event which I have to detect... only messure
some analoge input is possible.

## Primary Idea

Make arithmetic mean:

- using 3 intervals every **approx.** 5sec
- calucate speed value in each interval
- display arithmetic mean of 3 speed values 

What an event is:

- messure sensore value every 5ms
- magnet in weel comes next to the hall effect sensor
- messure value is bigger than a DIFF value: yes
  - count the event (roundi)
  - delay some miliseconds (combined + 50ms)
  - messure again to get a sensore value after the magnet disapears from the sensor

Calculate speed:

- we need WHEEL (perimeter of the circle in meters)
- we need the number of rotations in time (events)
- we need the exact time

Get exact time of an interval:

- without an event it is 5/3 seconds = 1.65 s
- every event takes 50ms (we have to add them to the time)

## Why so complicated?

- we need small mesure intervalls, because the magnet on a wheel moves very fast
- it is important to detect savely EVERY event, because in 5 seconds there are not so many of them
- We need a time window (45ms) when the event is over, because when the magnet
  comes (or goes) the sensor value changes permanently and could therefore
  count several events.
- messuring a peek of the sensor values (= as event) is much more code!

## Important

- please use trail and error to get good DIFF and WHEEL values
- do not forget: display something takes a delay in code!

![Circuit and Pinout](circuit.jpg)