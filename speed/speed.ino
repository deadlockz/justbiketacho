#include <TM1637Display.h>

// Display connection pins (Digital Pins)
#define CLK          11
#define DIO          12
#define SENSOR       A1

#define WHEEL   2.145 // meters (28")
#define DIFF    23    // please trail an error here

TM1637Display display(CLK, DIO);

int val    = 0;
int oldval = 0;

int ticks = 0;
float t = 0.0;

int roundi = 0;

float kmh1 = 0;
float kmh2 = 0;
float kmh3 = 0;
int   kmh  = 0;

void setup() {
  display.clear();
  display.setBrightness(0x0f);
}

void loop() {
  ticks = (ticks+1)%1000; // 1000x 5ms = 5sec
  val = analogRead(SENSOR);
  // we do not need +DIFF and -DIFF, because 1 round: it comes and goes!
  if (val-DIFF > oldval) {
    roundi++;
    // to count only a single event and give the magnet time to move away
    delay(45);
    val = analogRead(SENSOR);
  }
  oldval = val;
  delay(5);
  // intervall 0 ----------------
  if (ticks==0) {
    // but it was more than 1.65sec, because "roundi * 50ms" and not 333 * 5ms
    t = 1.65 + roundi * 0.05;
    kmh1 = roundi*WHEEL*3.6/t;
    roundi=0;
  }
  // intervall 1 ----------------
  if (ticks==333) {
    t = 1.65 + roundi * 0.05;
    kmh2 = roundi*WHEEL*3.6/t;
    roundi=0;
  }
  // intervall 2 ----------------
  if (ticks==667) {
    t = 1.65 + roundi * 0.05;
    kmh3 = roundi*WHEEL*3.6/t;
    roundi=0;
  }
  kmh = (int) round((kmh1+kmh2+kmh3)/3.0);

  // Display refresh -----------------
  if (ticks%67 == 0) {
    display.showNumberDec(kmh);
    // DEBUG
    // display.showNumberDecEx(roundi,0,false,2,0);
  }
}

